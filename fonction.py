import csv
import sqlite3

def creationBDD(cursor):
    cursor.execute('''CREATE TABLE IF NOT EXISTS auto (address TEXT, name TEXT, firstname TEXT, immat TEXT, dateimmat DATE, vin INTEGER, marque TEXT, denomination TEXT, couleur TEXT, carrosserie TEXT, categorie TEXT, cylindree TEXT, energy TEXT, places INTEGER, poids INTEGER, puissance INTEGER, type TEXT, variante TEXT, version INTEGER)''')

def valeurBDD(cursor):
    with open('voiture.csv', 'r') as incsv:
        reader = csv.DictReader(incsv, delimiter=';')
        for row in reader:
            a=row['address']
            b=row['name']
            c=row['firstname']
            d=row['immat']
            e=row['date_immat']
            f=row['vin']
            g=row['marque']
            h=row['denomination']
            i=row['couleur']
            j=row['carrosserie']
            k=row['categorie']
            l=row['cylindree']
            m=row['energy']
            n=row['places']
            o=row['poids']
            p=row['puissance']
            q=row['type']
            r=row['variante']
            s=row['version']
            tableau = [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s]
            if (is_in_database(cursor, f)==False):
                insertionBDD(cursor, tableau)
            

def printBDD(cursor):
    cursor.execute("SELECT * FROM auto")
    resultats = cursor.fetchall()
    for resultat in resultats:
        print (resultat)


def controlBDD(cursor):
    a=False
    if(cursor.execute("SELECT firstname FROM auto WHERE firstname='Diana'") != "('Diana',)"):
        a=True
    return a

def is_in_database(cursor, vin):
    result = cursor.execute('SELECT COUNT(vin) FROM auto WHERE vin = ?', (vin, )).fetchone()
    if result[0] == 0:
        return False
    return True

def insertionBDD(cursor, tableau):
    cursor.execute("INSERT INTO auto (address, name, firstname, immat, dateimmat, vin, marque, denomination, couleur, carrosserie, categorie, cylindree, energy, places, poids, puissance, type, variante, version) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", tableau)